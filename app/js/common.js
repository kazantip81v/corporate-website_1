$(document).ready(function() {
	
	$('nav a').click(function (event) {
		let arr = $(event.target).parentsUntil('body'),
			attrib;
		
		$('nav li').removeClass('active');
		$(this).parent().toggleClass('active');
		
		attrib = $('li.active a').attr('href');

		arr.each(function (index) {
			if (arr[index].nodeName === 'FOOTER') {
				$('header a[href=' + attrib + ']').parent().addClass('active');
			}
			if (arr[index].nodeName === 'HEADER') {
				$('footer a[href=' + attrib + ']').parent().addClass('active');
			}
		});
		
		$('nav').css('display', 'none');
		
	});
	
	$(".toggle-menu").click(function () {
			$(this).toggleClass("on");
			$(".main-menu").slideToggle();
	});

	$("footer .toggle-menu").click(function() {
		$("html, body").animate({ scrollTop: $(document).height() }, "slow");
		return false;
	});

	$(".arrow-up").click(function() {
		$("html, body").animate({ scrollTop: 0 }, "slow");
		return false;
	});

	$(".arrow-down").click(function() {
		$("html, body").animate({ scrollTop: $(".main-head").height()+125 }, "slow");
		return false;
	});

	$(".animate h2, .animate p").animated("fadeInUp");
	$(".info-item").animated("fadeInUp");
	$(".slider").animated("rollIn");
	//$(".section-main.section-8 .forms").animated("fadeInRight");
	//$(".section-8 h2, .section-8 p:first").animated("fadeIn");
	$(".s3-icon-wrap .icon-item").animated("rollIn");


	// waypoint - анимация в секции 2,4,5,6,8
	$(".section-2").waypoint(function () {
		$(".section-2 li").each(function (index) {
			var flag = $(this);
			setInterval(function () {
				flag.addClass("on");
			}, 200*index);
		});
	}, {
		offset: "35%"
	});

	$(".section-4").waypoint(function () {
		$(".section-4 .card").each(function (index) {
			var flag = $(this);
			setInterval(function () {
				flag.addClass("card-on");
			}, 200*index);
		});
	}, {
		offset: "25%"
	});

	$(".section-5").waypoint(function (dir) {
		if (dir === "down") {
			$(".section-5 .arrow-item").each(function (index) {
				var flag = $(this);
				setTimeout(function () {
					var myAnimation = new DrawFillSVG({
						elementId: "item-svg-" + index
					});
					flag.addClass("arrow-on");
				}, 400*index);
			});
		};
		this.destroy();
	}, {
		offset: "40%"
	});

	$(".section-6").waypoint(function () {
		$(".section-6 .card").each(function (index) {
			var flag = $(this);
			setInterval(function () {
				flag.addClass("card-on");
			}, 200*index);
		});
	}, {
		offset: "25%"
	});

/*	$(".section-8").waypoint(function () {
		$(".section-8 li").each(function (index) {
			var flag = $(this);
			setInterval(function () {
				flag.addClass("on");
			}, 150*index);
		});
	}, {
		offset: "35%"
	});*/

	// owl-carousel
	$(".slider").owlCarousel({
		items: 1,
		loop: true,
		nav: true,
		navText: "",
		autoplay: true,
		smartSpeed: 500,
		fluidSpeed: 500,
		autoplaySpeed: 700,
		navSpeed: 500,
		dotsSpeed: 500,
		dragEndSpeed: 500
	});


	//Цели для Яндекс.Метрики и Google Analytics
	$(".count_element").on("click", (function() {
		ga("send", "event", "goal", "goal");
		yaCounterXXXXXXXX.reachGoal("goal");
		return true;
	}));

	//SVG Fallback
	if(!Modernizr.svg) {
		$("img[src*='svg']").attr("src", function() {
			return $(this).attr("src").replace(".svg", ".png");
		});
	};

	// magnific-popup 
	$("a[href^='#callback']").click(function () {
		$("#callback h4").html($(this).text() + ":");
		$("#callback input[name='formName']").val($(this).text());
	}).magnificPopup({
  	type:'inline',
  	mainClass: 'mfp-forms'
	});

	//Аякс отправка форм
	//Документация: http://api.jquery.com/jquery.ajax/
	$(".forms").submit(function() {
		$.ajax({
			type: "POST",
			url: "/mail.php",
			data: $(this).serialize()
		}).done(function() {
			alert("Спасибо за заявку!");
			setTimeout(function() {
				
				$(".forms").trigger("reset");
			}, 1000);
		});
		return false;
	});

	//Chrome Smooth Scroll
	try {
		$.browserSelector();
		if($("html").hasClass("chrome")) {
			$.smoothScroll();
		}
	} catch(err) {

	};

	$("img, a").on("dragstart", function(event) { event.preventDefault(); });

});
